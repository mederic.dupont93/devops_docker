FROM ubuntu:groovy

RUN apt-get update && apt-get install curl=7.73.0 nginx=1.21.0 git=2.21 python=3.9.1 -y --no-install-recommends && apt-get clean && rm -rf /var/lib/apt/lists/*

RUN addgroup esgi

RUN useradd -rm -d /home/user -s /bin/bash -g user -u 1001 user

CMD ["sleep", "infinity"]

